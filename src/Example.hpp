/*
** Copyright (C) 2020 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-09-10T11:29:56+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#ifndef EXAMPLE_HPP__
#define EXAMPLE_HPP__

#include <cstdint>
#include <string>
#include <vector>

// Note: class and type names are always upper camel cased
class Example
{
public:
    // Note: declare local types at the top of the class
    typedef std::vector<std::int32_t> DataList;

    Example() = default;
    ~Example() = default;

    // Note: methods are always lower camel cased
    /**
     * @brief run the example
     * @return execution return code
     */
    int run();

    /**
     * @brief this method does something really complicated
     * @param[in] value input parameter, use const reference
     * @param[out] out output parameter, use reference
     * @return the sum of input elements
     *
     */
    std::int32_t processData(const DataList &value, DataList &out) const;

    // Note: consts are capital case
    // Note: non-const statics are prefixed with 's_'
    static const std::string VERSION;

    // Note: prefer protected over private, it is more unit-tests friendly
protected:
    // Note: do declare const properly
    /**
     * #brief generate some numbers
     * @param[out] out data list
     */
    void generateData(DataList &out) const;

    // Note: do use cstdint (or std::size_t)
    // Note: members are prefixed with 'm_'
    // Note: members are always lower camel cased
    std::int32_t m_value; /**<! a class attibute */
};

#endif
