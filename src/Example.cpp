/*
** Copyright (C) 2021 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-09-10T11:35:17+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include "Example.hpp"

#include <chrono>
#include <iostream>
#include <random>
#include <thread>

#include "local-config.h"

using std::chrono::milliseconds;

static const std::size_t s_dataSize(100);
static std::default_random_engine s_generator;
static std::uniform_int_distribution<int> s_distribution(0, 100);

const std::string Example::VERSION(VER);

void Example::generateData(std::vector<std::int32_t> &out) const
{
    out.resize(0);
    out.reserve(s_dataSize);
    for (std::size_t i = 0; i < s_dataSize; ++i)
    {
        out.emplace_back(s_distribution(s_generator));
    }
}

int Example::run()
{
    std::vector<std::int32_t> inData;
    std::vector<std::int32_t> outData;

    for (std::size_t i = 0; i < 100; ++i)
    {
        std::cout << "generating data..." << std::endl;
        generateData(inData);
        std::cout << "processing data: " << processData(inData, outData)
                  << std::endl;
        std::this_thread::sleep_for(milliseconds(10));
    }
    std::cout << "done" << std::endl;
    return 0;
}

std::int32_t Example::processData(const std::vector<std::int32_t> &value,
                                  std::vector<std::int32_t> &out) const
{
    std::int32_t ret = 0;
    out.resize(0);
    out.reserve(value.size());

    for (std::int32_t i : value)
    {
        ret += i;
        out.emplace_back(ret);
    }
    return ret;
}
