/*
** Copyright (C) 2021 CERN
**
** This software is provided 'as-is', without any express or implied
** warranty.  In no event will the authors be held liable for any damages
** arising from the use of this software.
**
** Permission is granted to anyone to use this software for any purpose,
** including commercial applications, and to alter it and redistribute it
** freely, subject to the following restrictions:
**
** 1. The origin of this software must not be misrepresented; you must not
**    claim that you wrote the original software. If you use this software
**    in a product, an acknowledgment in the product documentation would be
**    appreciated but is not required.
** 2. Altered source versions must be plainly marked as such, and must not be
**    misrepresented as being the original software.
** 3. This notice may not be removed or altered from any source distribution.
**
** Created on: 2021-09-10T11:55:10+02:00
**     Author: Sylvain Fargier <sfargier> <sylvain.fargier@cern.ch>
**
*/

#include <cppunit/TestFixture.h>

#include "Example.hpp"
#include "helpers_local.hpp"

class TestExample : public CppUnit::TestFixture
{
protected:
    CPPUNIT_TEST_SUITE(TestExample);
    CPPUNIT_TEST(simple);
    CPPUNIT_TEST(blackbox);
    CPPUNIT_TEST_SUITE_END();

public:
    void setUp() override
    {
        // Note: setup your environement here
    }

    void tearDown() override
    {
        // Note: clean your environment here
    }

    void simple()
    {
        Example ex;
        Example::DataList data{1, 2, 3, 4};
        Example::DataList out;

        EQ(std::int32_t(10), ex.processData(data, out));
        EQ(std::size_t(4), out.size());
        EQ((Example::DataList{1, 3, 6, 10}), out);
    }

    void blackbox()
    {
        Example ex;
        EQ(0, ex.run());
    }
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestExample);
